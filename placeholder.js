/*
 * Placeholder plugin
 * Dependencies: jQuery (tested up version v1.10.1)
 * Usage: Add ‘plchldr’ class to the form that needs placeholder functionality. Add desired placeholder to text inputs and textarea. Include this file after jQuery include and thats it.
 * Written by: Abhijeet Sidhu
 * URL: http://od14.com
 * Contact/Feedback/Concerns: abhijeet.sidhu@gmail.com
*/

(function () {
	if ($('.plchldr').length > 0) {
		var input = $('form.plchldr').find('input[type=text]');

      input.on("focus",function(e) {
          var placeholder = $(this).attr('placeholder');
          var val         = $(this).val();
          if (val == placeholder) {
              $(this).val('');
          }
      });

      input.on("blur",function(e) {
        var placeholder = $(this).attr('placeholder');
        var val         = $(this).val();
        if (val == '') {
            $(this).val(placeholder);
        }
      });

      input.blur();
    }
}());
